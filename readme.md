# About

This repo contains the code to create a set of basic maps of Spain, almost ready-to-use to visualize data (see this [post][2] for more details).

The folder is organized as follows:

* *data*: this directory contains some data files for visualization purposes;
* *images*: this directory contains, amog the others, the images shown throughout this file;
* *maps*: this directory contains the original maps (downloaded from [GADM][1]) used to get the basic geographical shapes (saved as `rds` objects, in the sub-directory *original*), as well as the modified maps;
* *scripts*: this directory stores the scripts used to transform the original geographical layers and those ones showing how to map data on the maps.

The data contained in the *data* folder have been downloaded from the Spanish Institute of Statistics ([INE][3]). 

Some of the images produced with the code contained in the *script* folder are:

![](images/spain_regions.png)

![](images/spain_provinces.png)

![](images/spainProv_geo_avgAge.png)

![](images/spainProv_avgAge.png)

[1]: https://www.gadm.org/
[2]: https://datarium.netlify.com/posts/190324_map_of_spain_ggplot2/
[3]: https://www.ine.es/
